// https://eth-ropsten.alchemyapi.io/v2/t1X3cbcEmyiD3menfa_9oMlAYItaCiQw

require('@nomiclabs/hardhat-waffle');

module.exports = {
  solidity: '0.8.0',
  networks: {
    ropsten: {
      url: 'https://eth-ropsten.alchemyapi.io/v2/t1X3cbcEmyiD3menfa_9oMlAYItaCiQw',
      accounts: [ 'b395154d76f6df34fc45dcba330e80fe5e6f8741a1ae262f98e70a0f86157d9f' ]
    }
  }
}